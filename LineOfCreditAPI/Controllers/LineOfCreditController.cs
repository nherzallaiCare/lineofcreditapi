﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LineOFCredit;
using LineOfCreditAPI.Repository;
using LineOfCreditAPI.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace LineOfCreditAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/LineOfCredit")]
    public class LineOfCreditController : Controller
    {
        ICreditLineRepository _creditLineRepository;
        IOptions<Settings> _settings;
        public LineOfCreditController(ICreditLineRepository creditLineRepository, IOptions<Settings> settings)
        {
            _creditLineRepository = creditLineRepository;
            _settings = settings;
        }
        [HttpPost("AddLineOfCredit")]
        public async Task<IActionResult> AddLineOfCredit([FromBody] LineOfCredit lineOfCredit)
        {
            if (lineOfCredit.CreditLimit > 0)
            {
                lineOfCredit.AvailableFund = lineOfCredit.CreditLimit;
                lineOfCredit.Balance = 0;
            }
            else
            {
                lineOfCredit.AvailableFund = 0;
                lineOfCredit.Balance = 0;
            }
            _creditLineRepository.DataBaseName = "LineOfCreditDB";
            _creditLineRepository.Initialize();

            await _creditLineRepository.AddCreditLineAsync(lineOfCredit);
            return Ok("Line Of Credit Added...");
        }
        [HttpGet("GetCustomer")]
        public IActionResult GetCustomer(string email)
        {
            _creditLineRepository.DataBaseName = "LineOfCreditDB";
            _creditLineRepository.Initialize();
            return Ok(_creditLineRepository.GetCustomerByEmail(email));

        }
        [HttpPut("UpdateBalance")]
        public IActionResult UpdateBalance([FromBody] LineOfCreditCommand lineOfCredit)
        {
            _creditLineRepository.DataBaseName = "LineOfCreditDB";
            _creditLineRepository.Initialize();

            var credit = _creditLineRepository.GetCustomerByEmail(lineOfCredit.Email);
            if (Convert.ToDouble(lineOfCredit.DrawAmount) > credit.AvailableFund || Convert.ToDouble(lineOfCredit.DrawAmount) > credit.CreditLimit)
            {
                return BadRequest("Your Draw Amount is higher than available fund or credit limit");
            }
            credit.AvailableFund = credit.CreditLimit - (Convert.ToDouble(lineOfCredit.DrawAmount)+ credit.Balance);
            credit.Balance = credit.Balance + Convert.ToDouble(lineOfCredit.DrawAmount);

            _creditLineRepository.UpdateCreditLine(credit);

            return Ok("Updated....");
              
        }
    }
}