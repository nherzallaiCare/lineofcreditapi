﻿
using LineOFCredit;
using LineOfCreditAPI.ViewModels;
using Microsoft.Extensions.Options;
using MongoDB.Driver;



namespace LineOfCreditAPI.DBContext
{

    public class LineOfCreditDBContext
    {
        private IMongoDatabase _database = null;

        public LineOfCreditDBContext(IOptions<Settings> settings, string Database)
        {

            var client = new MongoClient(settings.Value.ConnectionString);
            if (client != null)
            {
                _database = client.GetDatabase(Database);
            }
        }
        public IMongoCollection<LineOfCredit> LineOfCredit
        {
            get
            {
                return _database.GetCollection<LineOfCredit>("LineOfCredit");
            }
            set
            {
                _database.CreateCollection("LineOfCredit");
            }
        }
        public IMongoDatabase TemplateDataBase
        {
            get { return _database; }
        }
    }
}
