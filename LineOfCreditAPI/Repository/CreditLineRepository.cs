﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LineOFCredit;
using LineOfCreditAPI.DBContext;
using LineOfCreditAPI.ViewModels;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace LineOfCreditAPI.Repository
{
    public class CreditLineRepository : ICreditLineRepository
    {
        private LineOfCreditDBContext _context = null;
        private readonly IOptions<Settings> _settings;

        public string DataBaseName { get; set; }

        public CreditLineRepository(IOptions<Settings> settings)
        {
            _settings = settings;

            if (DataBaseName != null)
            {
                _context = new LineOfCreditDBContext(settings, DataBaseName);

            }
        }

        public void Initialize()
        {
            _context = new LineOfCreditDBContext(_settings, DataBaseName);
        }


        public LineOfCredit GetCustomerByEmail(string email)
        {
            var item = _context.LineOfCredit.AsQueryable().Where(q=>q.Email == email).FirstOrDefault();
            return item;

        }

         
        public async Task UpdateCreditLine(LineOfCredit lineOFCredit)
        {
            var builder = Builders<LineOfCredit>.Update.Set(c => c.AvailableFund, lineOFCredit.AvailableFund)
               .Set(c =>c.Balance,lineOFCredit.Balance);

            await _context.LineOfCredit.FindOneAndUpdateAsync(p => p.Id == lineOFCredit.Id, builder);
        }

        public async Task AddCreditLineAsync(LineOfCredit lineOFCredit)
        {
            await _context.LineOfCredit.InsertOneAsync(lineOFCredit);
        }
    }
}
