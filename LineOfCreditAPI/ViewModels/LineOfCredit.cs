﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LineOfCreditAPI.ViewModels
{
    public class LineOfCredit
    {
        public ObjectId Id
        {
            get; set;
        }
        public string BorrowerName { get; set; }
        public string Email { get; set; }
        public double CreditLimit { get; set; }
        public double Balance { get; set; }
        public double AvailableFund { get; set; }
        public double DrawAmount { get; set; }

    }
    public class LineOfCreditCommand
    {
        public string Email { get; set; }
        public string DrawAmount { get; set; }
    }
}
