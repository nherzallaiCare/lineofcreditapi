﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LineOfCreditAPI.ViewModels;


namespace LineOfCreditAPI.Repository
{
   public interface ICreditLineRepository
    {
        String DataBaseName { get; set; }
        void Initialize();
        Task UpdateCreditLine(LineOfCredit lineOFCredit);
        Task AddCreditLineAsync(LineOfCredit lineOFCredit);

        LineOfCredit GetCustomerByEmail(string email);
    }
}
